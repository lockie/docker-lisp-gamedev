#!/usr/bin/env bash

set -euxo pipefail

dir=$(dirname "$0")
backend=$1

export DEBIAN_FRONTEND=noninteractive
apt-get update -qq
apt-get install -y --no-install-recommends cookiecutter dbus git pipewire pipewire-media-session pipewire-pulse
/etc/init.d/dbus start
export DISABLE_RTKIT=y
export XDG_RUNTIME_DIR=/tmp/pipewire
export PIPEWIRE_RUNTIME_DIR=/tmp/pipewire
export PULSE_RUNTIME_DIR=/tmp/pipewire
mkdir -p $PIPEWIRE_RUNTIME_DIR
mkdir -p /dev/snd
pipewire &
pipewire-media-session &
pipewire-pulse &

WINEPREFIX=/tmp/prefix winecfg
ln -sf /root/.wine/drive_c/msys64 /tmp/prefix/drive_c/
WINEPREFIX=/tmp/prefix msys2 -c " "

cd /tmp
cookiecutter gh:lockie/cookiecutter-lisp-game --no-input --config-file "$dir/$backend.yml"
cd "test-$backend"
case $backend in
    allegro)
        sed -i '/(al:flip-display)/a\(return-from main-game-loop)' src/main.lisp
        ;;

    raylib)
        sed -i '/(render))/a\(return-from main-game-loop)' src/main.lisp
        ;;

    sdl2)
        sed -i '/(sdl2:render-present ren)/a\(return-from main)' src/main.lisp
    ;;
esac
msys2 -c "./package.sh windows"
WINEPREFIX=/tmp/prefix wine "./test-$backend--setup.exe" /S
sleep 10  # skipping that makes the game choke on video drivers :\
WINEPREFIX=/tmp/prefix timeout 30s msys2 -c "/c/users/root/AppData/Local/Programs/Test${backend^}/bin/test-$backend.exe"
