#!/usr/bin/env bash

set -euxo pipefail

dir=$(dirname "$0")
backend=$1

export DEBIAN_FRONTEND=noninteractive
add-apt-repository -yn ppa:pipewire-debian/pipewire-upstream
apt-get update -qq
apt-get install -y --no-install-recommends pipewire pipewire-media-session pipewire-pulse python3-pip xvfb
pip -qqq --no-cache-dir --disable-pip-version-check install cookiecutter
/etc/init.d/dbus start
export DISABLE_RTKIT=y
export XDG_RUNTIME_DIR=/tmp/pipewire
export PIPEWIRE_RUNTIME_DIR=/tmp/pipewire
export PULSE_RUNTIME_DIR=/tmp/pipewire
export ALSOFT_DRIVERS=pulse
mkdir -p $PIPEWIRE_RUNTIME_DIR
mkdir -p /dev/snd
pipewire &
pipewire-media-session &
pipewire-pulse &

cd /tmp
cookiecutter gh:lockie/cookiecutter-lisp-game --no-input --config-file "$dir/$backend.yml"
cd "test-$backend"
case $backend in
    allegro)
        sed -i '/(al:flip-display)/a\(return-from main-game-loop)' src/main.lisp
        ;;

    raylib)
        sed -i '/(render))/a\(return-from main-game-loop)' src/main.lisp
        ;;

    sdl2)
        sed -i '/(sdl2:render-present ren)/a\(return-from main)' src/main.lisp
    ;;
esac
./package.sh linux
timeout 30s xvfb-run -a "./test-$backend-.AppImage" --appimage-extract-and-run
